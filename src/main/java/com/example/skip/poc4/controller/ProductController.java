package com.example.skip.poc4.controller;

import com.example.skip.poc4.services.ProductService;
import com.example.skip.poc4.entities.Product;
import com.example.skip.poc4.exceptions.DBException;
import com.example.skip.poc4.exceptions.ErrorEnum;
import com.example.skip.poc4.exceptions.ErrorWrapper;
import com.example.skip.poc4.services.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * Rest controller api for Product entity CRUDs
 */
@RestController
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    private ProductService productService;
    private OrderService orderService;

    /**
     * Product controller initialization
     * @param productService (@link ProductService) product service
     * @param orderService (@link OrderService) order service
     */
    @Autowired
    public ProductController(final ProductService productService, final OrderService orderService) {
        this.productService = productService;
        this.orderService = orderService;
    }

    /**
     * Return all products in a list
     * @return (@link ResponseEntity) product list inside a ResponseEntity
     */
    @GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll() {
        return ResponseEntity.ok(productService.getAll());
    }

    /**
     * Retrieves an product for a given id
     * @param id identification of the product
     * @return a product wrapped in a response entity
     */
    @GetMapping(value = "/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getById(@PathVariable final Long id) {
        try {
            return ResponseEntity.ok(productService.getById(id));
        } catch (DBException e) {
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
     }

    /**
     * Saves an product in a database
     * @param product (@link product) to be saved
     * @return (@link ResponseEntity) the saved product inside ResponseEntity
     */
    @PostMapping(value = "/products", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody final Product product) {
        return ResponseEntity.ok(productService.save(product));
    }

    /**
     * Deletes an products in a databases
     * @param id (@link Long) for the product to be deleted
     * @return (@link ResponseEntity) a http response entity
     */
    @DeleteMapping(value = "/products/{id}")
    public ResponseEntity delete(@PathVariable final Long id) {
        try {
            Product product = productService.getById(id);
            productService.delete(id);
            Long orderId = product.getOrder().getId();
            orderService.delete(orderId);

            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (DBException e) {
            LOGGER.error(e.getMessage(), e);
            return new ResponseEntity(new ErrorWrapper(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
