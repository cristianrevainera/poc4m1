package com.example.skip.poc4.controller;


import com.example.skip.poc4.entities.Order;
import com.example.skip.poc4.exceptions.DBException;
import com.example.skip.poc4.services.OrderService;
import com.example.skip.poc4.exceptions.ErrorWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller api for Order entity CRUDs
 */
@RestController
public class OrderController {

    private OrderService orderService;

    /**
     * Order controller initialization
     * @param orderService {@link OrderService} injects OrderController dependencies
     */
    @Autowired
    public OrderController(final OrderService orderService) {
        this.orderService = orderService;
    }

    /**
     * Return all orders in a list
     * @return (@link ResponseEntity) order list inside a ResponseEntity
     */
    @GetMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll() {
        return ResponseEntity.ok(orderService.getAll());
    }

    /**
     * Retrieves an order for a given id
     * @param id identification of the order
     * @return (@link ResponseEntity) a order wrapped in a response entity
     */
    @GetMapping(value = "/orders/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getById(@PathVariable final Long id) {
        try {
            return ResponseEntity.ok(orderService.getById(id));
        } catch (DBException e) {
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Saves an order in a database
     * @param order (@link Order) to be saved
     * @return (@link ResponseEntity) the saved order
     */
    @PostMapping(value = "/orders", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody final Order order) {
       return ResponseEntity.ok(orderService.save(order));
    }

    /**
     * Deletes an order in a databases
     * @param id for the order to be deleted
     * @return (@link ResponseEntity) a http response entity
     */
    @DeleteMapping(value = "/orders/{id}")
    public ResponseEntity delete(@PathVariable final Long id) {
        orderService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
