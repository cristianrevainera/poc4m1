package com.example.skip.poc4.services;

import com.example.skip.poc4.entities.Order;
import com.example.skip.poc4.exceptions.DBException;

import java.util.List;

/**
 * Order Service layer definition
 */
public interface OrderService {

    /**
     * Retrives an order for a given identification number
     * @param id order's identification number
     * @return (@Link Order) an order object
     * @throws DBException throws a DBException is the object with the given id doesn't exist
     */
    Order getById(Long id) throws DBException;

    /**
     * Retrieves all orders
     * @return (@Link List<Order>) retrieves a list of Orders
     */
    List<Order> getAll();

    /**
     * Saves an order object in the database
     * @param order (@Link Order) order to be saved
     * @return (@Link Order) the saved order
     */
    Order save(Order order);

    /**
     * Deletes an order in the database
     * @param id (@Link Long) identification number of the order to be deleted)
     */
    void delete(Long id);

}
