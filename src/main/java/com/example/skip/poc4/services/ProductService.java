package com.example.skip.poc4.services;

import com.example.skip.poc4.entities.Product;
import com.example.skip.poc4.exceptions.DBException;

import java.util.List;

/**
 * Product service layer definition
 */
public interface ProductService {
    /**
     * Retrieves a Product with the given id
     * @param id (@Link Long) Product's identification number
     * @return (@Link Product) the found product
     * @throws DBException An exception if the product doesn't exist in the database
     */
    Product getById(Long id) throws DBException;

    /**
     * Retrieves all the products
     * @return (@Link Product) a list of products
     */
    List<Product> getAll();

    /**
     * Saves a product
     * @param product (@Link Product) product to be saved
     * @return (@Link Product) the saved product
     */
    Product save(Product product);

    /**
     * Deletes a product
     * @param id (@Long id) identification number of the product to be deleted
     */
    void delete(Long id);

}
