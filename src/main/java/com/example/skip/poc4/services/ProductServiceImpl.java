package com.example.skip.poc4.services;

import com.example.skip.poc4.entities.Product;
import com.example.skip.poc4.exceptions.DBException;
import com.example.skip.poc4.exceptions.ErrorEnum;
import com.example.skip.poc4.dao.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Product Service layer implementation
 */
@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    /**
     *
     * @param productRepository (@Link ProductRepository) injects ProductRepository dependencies
     */
    @Autowired
    public ProductServiceImpl(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Retrieves a Product with the given id
     * @param id (@Link Long) Product's identification number
     * @return (@Link Product) the found product
     * @throws DBException An exception if the product doesn't exist in the database
     */
    @Override
    public Product getById(final Long id) throws DBException {
        try {
            return productRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            throw new DBException(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg());
        }
    }

    /**
     * Retrieves all the products
     * @return (@Link Product) a list of products
     */
    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    /**
     * Saves a product
     * @param product (@Link Product) product to be saved
     * @return (@Link Product) the saved product
     */
    @Override
    public Product save(final Product product) {
        return productRepository.save(product);
    }

    /**
     * Deletes a product
     * @param id (@Long id) identification number of the product to be deleted
     */
    @Override
    public void delete(final Long id) {
        productRepository.delete(id);
    }

}
