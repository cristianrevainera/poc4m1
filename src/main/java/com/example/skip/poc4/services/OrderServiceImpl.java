package com.example.skip.poc4.services;

import com.example.skip.poc4.dao.OrderRepository;
import com.example.skip.poc4.entities.Order;
import com.example.skip.poc4.exceptions.DBException;
import com.example.skip.poc4.exceptions.ErrorEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Order Service layer implementation
 */
@Service
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;

    /**
     *
     * @param orderRepository (@Link OrderRepository) injects OrderRepository dependencies
     */
    @Autowired
    public OrderServiceImpl(final OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    /**
     * Retrives an order for a given identification number
     * @param id order's identification number
     * @return (@Link Order) an order object
     * @throws DBException throws a DBException is the object with the given id doesn't exist
     */
    @Override
    public Order getById(final Long id) throws DBException {
        try {
            return orderRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            throw new DBException(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg());
        }
    }

    /**
     * Retrieves all orders
     * @return (@Link List<Order>) retrieves a list of Orders
     */
    @Override
    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    /**
     * Saves an order object in the database
     * @param order (@Link Order) order to be saved
     * @return (@Link Order) the saved order
     */
    @Override
    public Order save(final Order order) {
        return orderRepository.save(order);
    }

    /**
     * Deletes an order in the database
     * @param id (@Link Long) identification number of the order to be deleted)
     */
    @Override
    public void delete(final Long id) {
        orderRepository.delete(id);
    }

}
