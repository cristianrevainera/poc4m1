package com.example.skip.poc4.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Order entity's database mapping class
 */
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    private String email;

    /**
     * Order entity identification number
     * @return order entity identification number
     */
    public Long getId() {
        return id;
    }

    /**
     * Change order entity identification number
     * @param id order entity identification number
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Retrieves de order entity's email
     * @return the email for the entity
     */
    public String getEmail() {
        return email;
    }

    /**
     * Change the order entity email
     * @param email of order entity
     */
    public void setEmail(final String email) {
        this.email = email;
    }
}
