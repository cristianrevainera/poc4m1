package com.example.skip.poc4.entities;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Product entity's database mapping class
 */
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    private Order order;

    /**
     * Product entity identification number
     * @return product entity identification number
     */
    public Long getId() {
        return id;
    }

    /**
     * Change product entity identification number
     * @param id (@Link Long) product entity identification number
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Retrieves the name of the product
     * @return (@Link String) product's name
     */
    public String getName() {
        return name;
    }

    /**
     * Change the product name
     * @param name (@Link String) name of the product
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Retrieves product's order
     * @return (@Link Order) an order entity
     */
    public Order getOrder() {
        return order;
    }

    /**
     * Change the product's order
     * @param order (@Link order) an order to relace in the product
     */
    public void setOrder(final Order order) {
        this.order = order;
    }


}
