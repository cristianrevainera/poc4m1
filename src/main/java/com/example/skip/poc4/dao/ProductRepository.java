package com.example.skip.poc4.dao;

import com.example.skip.poc4.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

/**
 * Database repository for product entity
 */
public interface ProductRepository extends JpaRepository<Product, Long> {

    /**
     * Delete a product from repository
     * @param id identification number of the product to be deleted
     */
    @Transactional
    @Modifying
    @Query("delete from Product where id = ?1")
    void delete(Long id);
}
