package com.example.skip.poc4.dao;

import com.example.skip.poc4.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

/**
 * Database repository for Order entity
 */
public interface OrderRepository extends JpaRepository<Order, Long> {

    /**
     * Delete an order from repository
     * @param id identification number of the order to be deleted
     */
    @Transactional
    @Modifying
    @Query("delete from Order where id = ?1")
    void delete(Long id);
}
