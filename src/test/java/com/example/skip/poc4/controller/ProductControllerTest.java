package com.example.skip.poc4.controller;

import com.example.skip.poc4.entities.Order;
import com.example.skip.poc4.entities.Product;
import com.example.skip.poc4.exceptions.DBException;
import com.example.skip.poc4.exceptions.ErrorEnum;
import com.example.skip.poc4.exceptions.ErrorWrapper;
import com.example.skip.poc4.services.OrderService;
import com.example.skip.poc4.services.OrderServiceImpl;
import com.example.skip.poc4.services.ProductService;
import com.example.skip.poc4.services.ProductServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class ProductControllerTest {

    private ProductService productServiceMock;
    private OrderService orderServiceMock;
    private ProductController productController;
    private Product product1;
    private String product1Name;
    private Long product1Id;

    private Order order1;
    private String order1Email;
    private Long order1Id;

    @Rule
    public ExpectedException exception=ExpectedException.none();

    @Before
    public void setup() {
        productServiceMock = mock(ProductServiceImpl.class);
        orderServiceMock = mock(OrderServiceImpl.class);
        productController = new ProductController(productServiceMock, orderServiceMock);

        order1Email = "test@gmail.com";
        order1Id = 1L;
        order1 = new Order();
        order1.setEmail(order1Email);
        order1.setId(order1Id);

        product1Id = 1L;
        product1Name = "phone";
        product1 = new Product();
        product1.setId(1L);
        product1.setName(product1Name);
        product1.setOrder(order1);
    }

    @Test
    public void productControllerGetAll() {
        List<Product> products = Arrays.asList(product1);
        doReturn(products).when(productServiceMock).getAll();

        ResponseEntity responseEntity = productController.getAll();
        List<Product> productsReturned = (List<Product>) responseEntity.getBody();

        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCodeValue());

        assertNotNull(productsReturned);
        assertEquals(1, productsReturned.size());
        assertEquals(product1Name, productsReturned.get(0).getName());
        assertEquals(product1Id, productsReturned.get(0).getId());
    }

    @Test
    public void productControllerGetById() throws DBException {
        doReturn(product1).when(productServiceMock).getById(product1Id);
        ResponseEntity responseEntity = productController.getById(product1Id);

        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCodeValue());

        Product productReturned = (Product) responseEntity.getBody();
        assertEquals(product1Id, productReturned.getId());
        assertEquals(product1Name, productReturned.getName());
    }

    @Test
    public void productControllerGetByIdFail() throws DBException {

        Throwable throwableMock = mock(Throwable.class);
        final DBException dbException = new DBException(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg(), throwableMock);

        doThrow(dbException).when(productServiceMock).getById(anyLong());

        ResponseEntity responseEntity = productController.getById(product1Id);
        ErrorWrapper errorWrapper = (ErrorWrapper) responseEntity.getBody();

        assertEquals(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg(), errorWrapper.getMessage());
    }

    @Test
    public void productControllerSave() {

        Product savedProduct = new Product();
        savedProduct.setName(product1.getName());
        savedProduct.setId(product1.getId());

        doReturn(savedProduct).when(productServiceMock).save(product1);

        ResponseEntity productRetrievedResponseEntity = productController.save(product1);
        Product productRetrieved = (Product) productRetrievedResponseEntity.getBody();

        assertEquals(productRetrieved.getName(), product1.getName());
        assertEquals(HttpStatus.OK.value(), productRetrievedResponseEntity.getStatusCode().value());
    }


    @Test
    public void delete() throws DBException {

        doReturn(product1).when(productServiceMock).getById(product1Id);

        doNothing().when(productServiceMock).delete(product1Id);
        doNothing().when(orderServiceMock).delete(order1Id);

        ResponseEntity retrievedResponseEntity = productController.delete(product1Id);
        assertEquals(HttpStatus.OK.value(), retrievedResponseEntity.getStatusCode().value());
    }

    @Test
    public void deleteFail() throws DBException {
        final DBException dbException = new DBException(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg());

        doThrow(dbException).when(productServiceMock).getById(anyLong());

        ResponseEntity responseEntity = productController.delete(product1Id);
        ErrorWrapper errorWrapper = (ErrorWrapper) responseEntity.getBody();

        assertEquals(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg(), errorWrapper.getMessage());
    }
}
