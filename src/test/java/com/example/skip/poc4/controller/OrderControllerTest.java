package com.example.skip.poc4.controller;

import com.example.skip.poc4.entities.Order;
import com.example.skip.poc4.exceptions.DBException;
import com.example.skip.poc4.exceptions.ErrorEnum;
import com.example.skip.poc4.services.OrderServiceImpl;
import com.example.skip.poc4.exceptions.ErrorWrapper;
import com.example.skip.poc4.services.OrderService;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;


public class OrderControllerTest {

    private OrderService orderServiceMock;
    private OrderController orderController;
    private Order order1;
    private String order1Email;
    private Long order1Id;

    @Rule
    public ExpectedException exception=ExpectedException.none();

    @Before
    public void setup() {
        orderServiceMock = mock(OrderServiceImpl.class);
        orderController = new OrderController(orderServiceMock);

        order1Id = 1L;
        order1Email = "test@globant.com";
        order1 = new Order();
        order1.setId(1L);
        order1.setEmail(order1Email);
    }

    @Test
    public void getAll() {
        List<Order> orders = Arrays.asList(order1);
        doReturn(orders).when(orderServiceMock).getAll();

        ResponseEntity responseEntity = orderController.getAll();

        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCodeValue());

        List<Order> ordersReturned = (List<Order>) responseEntity.getBody();
        assertNotNull(ordersReturned);
        assertEquals(1, ordersReturned.size());
        assertEquals(order1Email, ordersReturned.get(0).getEmail());
        assertEquals(order1Id, ordersReturned.get(0).getId());
    }

    @Test
    public void getById() throws DBException {
        doReturn(order1).when(orderServiceMock).getById(order1Id);
        ResponseEntity responseEntity = orderController.getById(order1Id);

        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCodeValue());

        Order orderReturned = (Order) responseEntity.getBody();
        assertEquals(order1Id, orderReturned.getId());
        assertEquals(order1Email, orderReturned.getEmail());
    }

    @Test
    public void getByIdFail() throws DBException {

        final DBException dbException = new DBException(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg());

        doThrow(dbException).when(orderServiceMock).getById(anyLong());

        ResponseEntity responseEntity = orderController.getById(order1Id);
        ErrorWrapper errorWrapper = (ErrorWrapper) responseEntity.getBody();

        assertEquals(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg(), errorWrapper.getMessage());
     }

     @Test
     public void save() {

        Order savedOrder = new Order();
        savedOrder.setEmail(order1.getEmail());
        savedOrder.setId(order1.getId());

        doReturn(savedOrder).when(orderServiceMock).save(order1);

        ResponseEntity orderRetrievedResponseEntity = orderController.save(order1);
        Order orderRetrieved = (Order) orderRetrievedResponseEntity.getBody();

        assertEquals(orderRetrieved.getEmail(), order1.getEmail());
        assertEquals(HttpStatus.OK.value(), orderRetrievedResponseEntity.getStatusCode().value());
     }

     @Test
     public void delete() {
        doNothing().when(orderServiceMock).delete(order1Id);

        ResponseEntity retrievedResponseEntity = orderController.delete(order1Id);
        assertEquals(HttpStatus.OK.value(), retrievedResponseEntity.getStatusCode().value());
      }
}
