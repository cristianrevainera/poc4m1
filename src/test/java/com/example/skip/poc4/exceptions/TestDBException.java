package com.example.skip.poc4.exceptions;


import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class TestDBException {

    @Test
    public void messageConstructor() {
        DBException dbException = new DBException(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg());
        assertThat(dbException, is(notNullValue()));
    }

    @Test
    public void messageAndThrowableConstructor() {
        Throwable throwableMock = mock(Throwable.class);
        DBException dbException2 = new DBException(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg(), throwableMock);
        assertThat(dbException2, is(notNullValue()));
    }
}
