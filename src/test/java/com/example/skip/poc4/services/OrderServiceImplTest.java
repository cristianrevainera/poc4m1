package com.example.skip.poc4.services;

import com.example.skip.poc4.entities.Order;
import com.example.skip.poc4.exceptions.DBException;
import com.example.skip.poc4.exceptions.ErrorEnum;
import com.example.skip.poc4.dao.OrderRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;


public class OrderServiceImplTest {

    private OrderRepository orderRepositoryMock;
    private Order order1;
    private String order1Email;
    private Long order1Id;

    private OrderService orderService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        orderRepositoryMock = mock(OrderRepository.class);
        orderService = new OrderServiceImpl(orderRepositoryMock);

        order1Id = 1L;
        order1Email = "test@globant.com";
        order1 = new Order();
        order1.setId(order1Id);
        order1.setEmail(order1Email);
    }

    @Test
    public void getAll() {

        List<Order> orders = Arrays.asList(order1);
        doReturn(orders).when(orderRepositoryMock).findAll();

        List<Order> ordersRetrieved = orderService.getAll();

        assertFalse(ordersRetrieved.isEmpty());
        assertEquals(1, ordersRetrieved.size());
        assertEquals(order1Email, ordersRetrieved.get(0).getEmail());
    }

    @Test
    public void getById() throws DBException {

        Optional optionalMock = Optional.of(order1); //mock(Optional.class);
        doReturn(optionalMock).when(orderRepositoryMock).findById(order1Id);
        // doReturn(order1).when(optionalMock).get();

        Order orderReturned = orderService.getById(order1Id);

        assertEquals(order1Id, orderReturned.getId());
        assertEquals(order1Email, orderReturned.getEmail());
    }


    @Test
    public void getByIdFail() throws DBException {

        thrown.expect(DBException.class);
        thrown.expectMessage(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg());

        final DBException dbException = new DBException(ErrorEnum.REGISTRY_NOT_FOUND_ERROR.getMsg());

        final NoSuchElementException noSuchElementException = new NoSuchElementException();

        doThrow(noSuchElementException).when(orderRepositoryMock).findById(order1Id);
        orderService.getById(order1Id);
    }

    @Test
    public void save() {
        Order orderSaved = new Order();
        orderSaved.setId(order1.getId());
        orderSaved.setEmail(order1.getEmail());

        doReturn(orderSaved).when(orderRepositoryMock).save(order1);

        Order returnedOrder = orderService.save(order1);

        //assertNotNull(returnedOrder);
        assertEquals(order1Email, returnedOrder.getEmail());
        assertEquals(order1Id, returnedOrder.getId());
    }

    @Test
    public void delete() {
        doNothing().when(orderRepositoryMock).delete(order1Id);
        orderService.delete(order1Id);
        verify(orderRepositoryMock, times(1)).delete(order1Id);
    }
}
