package com.example.skip.poc4.services;

import com.example.skip.poc4.dao.ProductRepository;
import com.example.skip.poc4.entities.Order;
import com.example.skip.poc4.entities.Product;
import com.example.skip.poc4.exceptions.DBException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

public class ProductServiceImplTest {

    private ProductRepository productRepositoryMock;
    private Product product1;
    private String product1Name;
    private Long product1Id;
    private Order order1;
    private String order1Email;
    private Long order1Id;


    private ProductService productService;

    @Before
    public void setup() {
        productRepositoryMock = mock(ProductRepository.class);
        productService = new ProductServiceImpl(productRepositoryMock);

        product1Id = 1L;
        product1Name = "phone";
        product1 = new Product();
        product1.setId(product1Id);
        product1.setName(product1Name);

        order1Id = 1L;
        order1Email = "test@globant.com";
        order1 = new Order();
        order1.setId(order1Id);
        order1.setEmail(order1Email);
        product1.setOrder(order1);
    }

    @Test
    public void getAll() {

        List<Product> products = Arrays.asList(product1);
        doReturn(products).when(productRepositoryMock).findAll();

        List<Product> productsRetrieved = productService.getAll();

        assertFalse(productsRetrieved.isEmpty());
        assertEquals(1, productsRetrieved.size());
        assertEquals(product1Name, productsRetrieved.get(0).getName());
        assertEquals(order1Email, productsRetrieved.get(0).getOrder().getEmail());
    }

    @Test
    public void save() {
        Product productSaved = new Product();
        productSaved.setId(product1.getId());
        productSaved.setName(product1.getName());
        productSaved.setOrder(order1);

        doReturn(productSaved).when(productRepositoryMock).save(product1);

        Product returnedProduct = productService.save(product1);

        //assertNotNull(returnedOrder);
        assertEquals(product1Name, returnedProduct.getName());
        assertEquals(product1Id, returnedProduct.getId());
        assertEquals(order1Email, returnedProduct.getOrder().getEmail());
    }

    @Test
    public void delete() {
        doNothing().when(productRepositoryMock).delete(product1Id);
        productService.delete(product1Id);
        verify(productRepositoryMock, times(1)).delete(product1Id);
    }

//    @Test @Ignore
//    public void getById() throws DBException {
//    }

    @Test(expected = DBException.class)
    public void getByIdFail() throws DBException {

        final NoSuchElementException noSuchElementException = new NoSuchElementException();

        doThrow(noSuchElementException).when(productRepositoryMock).findById(order1Id);
        productService.getById(order1Id);
    }
}
